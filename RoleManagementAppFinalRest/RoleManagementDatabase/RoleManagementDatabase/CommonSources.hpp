#pragma once
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <map>
#include <regex>

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/config.hpp>
#include <document.h>
#include <writer.h>
#include <stringbuffer.h>
#include <schema.h>
#include <fstream>

#include "mysql-connector-c++-8.0.26-win32/include/jdbc/mysql_connection.h"
#include "mysql-connector-c++-8.0.26-win32/include/jdbc/cppconn/driver.h"
#include "mysql-connector-c++-8.0.26-win32/include/jdbc/cppconn/exception.h"
#include "mysql-connector-c++-8.0.26-win32/include/jdbc/cppconn/resultset.h"
#include "mysql-connector-c++-8.0.26-win32/include/jdbc/cppconn/statement.h"
#include "mysql-connector-c++-8.0.26-win32/include/jdbc/cppconn/prepared_statement.h"


#include "prettywriter.h"
#include "stringbuffer.h"
#include "ostreamwrapper.h"

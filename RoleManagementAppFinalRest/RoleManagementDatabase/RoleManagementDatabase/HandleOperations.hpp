#pragma once
#include "CommonSources.hpp"
class HandleOperations
{
public:
	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::ResultSet *resGetContainer, *resAddContainer;
	sql::PreparedStatement *pstmGetContainer, *pstmAddContainer;
	void getContainer();
	void addContainer();
	void addContainerStoredProcedure();
	void updateParentContainer();
	void deleteContainerWithTransaction();
	void getRole();
	void addRole();
	void updateRole();
	void deleteRole();
	void testTransaction();
	void getPermissionsForRole();
	void getRoleForPermission();
	void addPermissionAssignment();
	void deletePermissionAssignment();
	HandleOperations();
	~HandleOperations();
};


#include "Roles.hpp"
namespace http = boost::beast::http;
using std::string;

Roles::Roles(const HandleOperations& con)
	: connectedToDB{ con }
{
	std::cout << "Calling constructor of roles\n";
}
//BACKUP
//boost::beast::http::status Roles::getRole(Document &docJson, std::map<std::string, std::string> &queryParameters)
//{
//	HandleOperations con;
//	const char* r_json = "{\"roles\":[]}";
//	Document doc;
//	docJson.Parse(r_json);
//	Value& a = docJson["roles"];
//	Document::AllocatorType& allocator = docJson.GetAllocator();
//	try {
//		std::auto_ptr<sql::Statement> stmtGetRole(connectedToDB.con->createStatement());
//		stmtGetRole->execute("CALL get_role()");
//		std::auto_ptr< sql::ResultSet > res;
//			do {
//				res.reset(stmtGetRole->getResultSet());
//				while (res->next()) {
//					Value obj(kObjectType), namev, parentv;
//					std::string name = res->getString("role_name");
//					std::string parentcont = res->getString("parent_container_name");
//					//		std::cout << "The name is " << name << " and the parent container is " << parentcont << std::endl;
//					namev.SetString(name.c_str(), allocator);
//					parentv.SetString(parentcont.c_str(), allocator);
//					obj.AddMember("roleName", namev, allocator);
//					obj.AddMember("parentContainer", parentv, allocator);
//					a.PushBack(obj, allocator);
//				}
//				/*	for (SizeType i = 0; i < a.Size(); ++i) {
//						std::string roleName = a[i]["roleName"].GetString();
//						std::string roleContainerID = a[i]["parentContainer"].GetString();
//						std::cout << "The parrent container for " << roleName << " is " << roleContainerID << std::endl;
//					}*/
//			} while (stmtGetRole->getMoreResults());
//		StringBuffer buffer;
//		PrettyWriter<StringBuffer> writer(buffer);
//		docJson.Accept(writer);
//	}
//	catch (sql::SQLException &e) {
//		std::cout << "# ERR: SQLException in " << __FILE__;
//		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
//		std::cout << "# ERR: " << e.what();
//		std::cout << " (MySQL error code: " << e.getErrorCode();
//		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
//	}
//	return http::status::ok;
//}

boost::beast::http::status Roles::getRole(Document &docJson, std::map<std::string, std::string> &queryParameters)
{
	HandleOperations con;
	const char* r_json = "{\"roles\":[]}";
	Document doc;
	docJson.Parse(r_json);
	Value& a = docJson["roles"];
	Document::AllocatorType& allocator = docJson.GetAllocator();

	std::string aPageSize{}, aPageNumber{};
	if (!r.isPageAndSizeNumber(queryParameters, aPageSize, aPageNumber)) {
		r.sendErrorMessage(docJson, 400, "Page or pageSize is not a number");
		return http::status::bad_request;
	}
	std::vector<std::string> vect = r.getPageSizeAndNumber(queryParameters, aPageSize, aPageNumber);
	try {
		std::auto_ptr<sql::Statement> stmtGetRole(connectedToDB.con->createStatement());
		if (queryParameters.empty()) {
			stmtGetRole->execute("CALL get_role()");
		}
		else
		{
			std::string str = "CALL get_role_limit(" + vect[0] + "," + vect[1] + ")";
			stmtGetRole->execute(str);
		}
		std::auto_ptr< sql::ResultSet > res;
		do {
			res.reset(stmtGetRole->getResultSet());
			while (res->next()) {
				Value obj(kObjectType), idV, nameV, startDateV, endDateV, riskLevelV, roleTypeV, descriptionV, parentV;
				std::string id = res->getString("role_id");
				std::string name = res->getString("role_name");
				std::string start_date = res->getString("start_date");
				std::string end_date = res->getString("end_date");
				std::string risk_level = res->getString("risk_level");
				std::string role_type = res->getString("role_type");
				std::string description = res->getString("description");
				std::string parent_cont = res->getString("parent_container_name");
				idV.SetString(id.c_str(), allocator);
				nameV.SetString(name.c_str(), allocator);
				startDateV.SetString(start_date.c_str(), allocator);
				endDateV.SetString(end_date.c_str(), allocator);
				riskLevelV.SetString(risk_level.c_str(), allocator);
				roleTypeV.SetString(role_type.c_str(), allocator);
				descriptionV.SetString(description.c_str(), allocator);
				parentV.SetString(parent_cont.c_str(), allocator);

				obj.AddMember("roleID", idV, allocator);
				obj.AddMember("roleName", nameV, allocator);
				obj.AddMember("startDate", startDateV, allocator);
				obj.AddMember("endDate", endDateV, allocator);
				obj.AddMember("riskLevel", riskLevelV, allocator);
				obj.AddMember("roleType", roleTypeV, allocator);
				obj.AddMember("description", descriptionV, allocator);
				obj.AddMember("parentContainer", parentV, allocator);
				a.PushBack(obj, allocator);
			}

		} while (stmtGetRole->getMoreResults());
		StringBuffer buffer;
		PrettyWriter<StringBuffer> writer(buffer);
		docJson.Accept(writer);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
	return http::status::ok;
}


boost::beast::http::status Roles::deleteRole(Document &docJson, std::string &roleId)
{
	try {
		std::auto_ptr<sql::Statement> stmtDeleteRole(connectedToDB.con->createStatement());
		std::string str = "CALL delete_role(" + roleId + ")";
		stmtDeleteRole->execute(str);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
	r.sendErrorMessage(docJson, 200, "Role Deleted");
	//	RestUtils::sendErrorMessage(docJson, 200, "Role Deleted");
	return http::status::ok;
}

// ADD ROLE WITH IMPUT FROM KEYBOARD

//boost::beast::http::status Roles::addRole(const std::string &postBody, Document &docJson, std::string &locationField,
//	const std::string &resource, std::string &errResponse)
//{
//	std::string roleName{}, startDate{}, endDate{}, riskLevel{}, roleType{}, parentContainerId{}, description{};
//	std::cout << "Enter name of your new role: ";
//	std::getline(std::cin, roleName);
//	std::cout << "Enter the start date for the role: ";
//	std::getline(std::cin, startDate);
//	std::cout << "Enter the end date for the role: ";
//	std::getline(std::cin, endDate);
//	std::cout << "Enter the risk level for the role: ";
//	std::getline(std::cin, riskLevel);
//	std::cout << "Enter the role type for the role: ";
//	std::getline(std::cin, roleType);
//	std::cout << "Enter the parent container id for the role: ";
//	std::getline(std::cin, parentContainerId);
//	std::cout << "Enter the description for the role: ";
//	std::getline(std::cin, description);
//	try {
//		std::auto_ptr<sql::Statement> stmtAddRole(connectedToDB.con->createStatement());
//		std::string str = "CALL add_role('" + roleName + "','" + startDate + "','" + endDate
//			+ "','" + riskLevel + "','" + roleType + "','" + parentContainerId + "','" + description + "')";
//		stmtAddRole->execute(str);
//	}
//	catch (sql::SQLException &e) {
//		std::cout << "# ERR: SQLException in " << __FILE__;
//		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
//		std::cout << "# ERR: " << e.what();
//		std::cout << " (MySQL error code: " << e.getErrorCode();
//		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
//	}
//	return http::status::created;
//}


boost::beast::http::status Roles::addRole(const std::string &postBody, Document &docJson, std::string &locationField,
	const std::string &resource, std::string &errResponse)
{
	std::string roleName{}, startDate{}, endDate{}, riskLevel{}, roleType{}, parentContainerId{}, description{};
	Document d;
	d.Parse(postBody.c_str());

	const char* r_json = "{\"role\":[]}";
	Document doc;
	docJson.Parse(r_json);
	Value& a = docJson["role"];
	Document::AllocatorType& allocator = docJson.GetAllocator();

	if (d.HasMember("name")) {
		roleName = d["name"].GetString();
		if (roleName.empty())
		{
			r.sendErrorMessage(docJson, 400, "The role name field is empty");
			return http::status::bad_request;
		}
	}

	if (d.HasMember("start_date")) {
		startDate = d["start_date"].GetString();
	}

	if (d.HasMember("end_date")) {
		endDate = d["end_date"].GetString();
	}

	if (d.HasMember("risk_level")) {
		riskLevel = d["risk_level"].GetString();
		if ((riskLevel != "low") && (riskLevel != "medium") && (riskLevel != "high"))
		{
			r.sendErrorMessage(docJson, 400, "The risk level is invalid!");
			return http::status::bad_request;
		}
	}

	if (d.HasMember("role_type")) {
		roleType = d["role_type"].GetString();
	}

	if (d.HasMember("parent_container_id")) {
		parentContainerId = d["parent_container_id"].GetString();
	}

	if (d.HasMember("description")) {
		description = d["description"].GetString();
	}

	// create value objects
	Value obj(kObjectType), nameV, startDateV, endDateV, riskLevelV, roleTypeV, parentContainerIdV, descriptionV;
	// set string 
	nameV.SetString(roleName.c_str(), allocator);
	startDateV.SetString(startDate.c_str(), allocator);
	endDateV.SetString(endDate.c_str(), allocator);
	riskLevelV.SetString(riskLevel.c_str(), allocator);
	roleTypeV.SetString(roleType.c_str(), allocator);
	parentContainerIdV.SetString(parentContainerId.c_str(), allocator);
	descriptionV.SetString(description.c_str(), allocator);
	// add members to obj
	obj.AddMember("roleName", nameV, allocator);
	obj.AddMember("startDate", startDateV, allocator);
	obj.AddMember("endDate", endDateV, allocator);
	obj.AddMember("riskLevel", riskLevelV, allocator);
	obj.AddMember("roleType", roleTypeV, allocator);
	obj.AddMember("parentContainerId", parentContainerIdV, allocator);
	obj.AddMember("description", descriptionV, allocator);
	a.PushBack(obj, allocator);

	try {
		std::auto_ptr<sql::Statement> stmtAddRole(connectedToDB.con->createStatement());
		std::string str = "CALL add_role('" + roleName + "','" + startDate + "','" + endDate
			+ "','" + riskLevel + "','" + roleType + "','" + parentContainerId + "','" + description + "')";
		stmtAddRole->execute(str);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}

	StringBuffer buffer;
	PrettyWriter<StringBuffer> writer(buffer);
	docJson.Accept(writer);

	return http::status::created;
}


// UPDATE ROLE WITH IMPUT FROM KEYBOARD
//
//boost::beast::http::status Roles::updateRole(const std::string &postBody, Document &docJson, const std::string &appID) 
//{
//	std::string id{}, updatedRoleName{}, updatedStartDate{}, updatedEndDate{}, updatedRiskLevel{}, updatedRoleType{}, updatedParentContainerId{}, updatedDescription{};
//	//std::cout << "Enter the id of the role you want to update:";
//	//std::getline(std::cin, id);
//	std::cout << "Enter the updated name for the role:";
//	std::getline(std::cin, updatedRoleName);
//	std::cout << "Enter the updated start date for the role:";
//	std::getline(std::cin, updatedStartDate);
//	std::cout << "Enter the updated end date for the role:";
//	std::getline(std::cin, updatedEndDate);
//	std::cout << "Enter the updated risk level for the role:";
//	std::getline(std::cin, updatedRiskLevel);
//	std::cout << "Enter the updated role type for the role:";
//	std::getline(std::cin, updatedRoleType);
//	std::cout << "Enter the updated parent container id for the role:";
//	std::getline(std::cin, updatedParentContainerId);
//	std::cout << "Enter the description for the role:";
//	std::getline(std::cin, updatedDescription);
//	try {
//		std::auto_ptr<sql::Statement> stmtAddRole(connectedToDB.con->createStatement());
//		std::string str = "CALL update_role('" + appID + "','" + updatedRoleName + "','" + updatedStartDate + "','" + updatedEndDate
//			+ "','" + updatedRiskLevel + "','" + updatedRoleType + "','" + updatedParentContainerId + "','" + updatedDescription + "')";
//		stmtAddRole->execute(str);
//	}
//	catch (sql::SQLException &e) {
//		std::cout << "# ERR: SQLException in " << __FILE__;
//		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
//		std::cout << "# ERR: " << e.what();
//		std::cout << " (MySQL error code: " << e.getErrorCode();
//		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
//	}
//	return http::status::ok;
//}


boost::beast::http::status Roles::updateRole(const std::string &postBody, Document &docJson, const std::string &appID)
{
	std::string id{}, updatedRoleName{}, updatedStartDate{}, updatedEndDate{}, updatedRiskLevel{}, updatedRoleType{}, updatedParentContainerId{}, updatedDescription{};
	Document d;
	d.Parse(postBody.c_str());

	const char* r_json = "{\"role\":[]}";
	Document doc;
	docJson.Parse(r_json);
	Value& a = docJson["role"];
	Document::AllocatorType& allocator = docJson.GetAllocator();

	if (d.HasMember("name")) {
		updatedRoleName = d["name"].GetString();
		if (updatedRoleName.empty())
		{
			r.sendErrorMessage(docJson, 400, "The role name field is empty");
			return http::status::bad_request;
		}
	}

	if (d.HasMember("name")) {
		updatedRoleName = d["name"].GetString();
	}

	if (d.HasMember("start_date")) {
		updatedStartDate = d["start_date"].GetString();
	}

	if (d.HasMember("end_date")) {
		updatedEndDate = d["end_date"].GetString();
	}

	if (d.HasMember("risk_level")) {
		updatedRiskLevel = d["risk_level"].GetString();
		if ((updatedRiskLevel != "low") && (updatedRiskLevel != "medium") && (updatedRiskLevel != "high"))
		{
			r.sendErrorMessage(docJson, 400, "The risk level is invalid!");
			return http::status::bad_request;
		}
	}

	if (d.HasMember("role_type")) {
		updatedRoleType = d["role_type"].GetString();
	}

	if (d.HasMember("parent_container_id")) {
		updatedParentContainerId = d["parent_container_id"].GetString();
	}

	if (d.HasMember("description")) {
		updatedDescription = d["description"].GetString();
	}

	// create value objects
	Value obj(kObjectType), updatedRoleNameV, updatedStartDateV, updatedEndDateV, updatedRiskLevelV, updatedRoleTypeV, updatedParentContainerIdV, updatedDescriptionV;
	// set string 
	updatedRoleNameV.SetString(updatedRoleName.c_str(), allocator);
	updatedStartDateV.SetString(updatedStartDate.c_str(), allocator);
	updatedEndDateV.SetString(updatedEndDate.c_str(), allocator);
	updatedRiskLevelV.SetString(updatedRiskLevel.c_str(), allocator);
	updatedRoleTypeV.SetString(updatedRoleType.c_str(), allocator);
	updatedParentContainerIdV.SetString(updatedParentContainerId.c_str(), allocator);
	updatedDescriptionV.SetString(updatedDescription.c_str(), allocator);
	// add members to obj
	obj.AddMember("roleName", updatedRoleNameV, allocator);
	obj.AddMember("startDate", updatedStartDateV, allocator);
	obj.AddMember("endDate", updatedEndDateV, allocator);
	obj.AddMember("riskLevel", updatedRiskLevelV, allocator);
	obj.AddMember("roleType", updatedRoleTypeV, allocator);
	obj.AddMember("parentContainerId", updatedParentContainerIdV, allocator);
	obj.AddMember("description", updatedDescriptionV, allocator);
	a.PushBack(obj, allocator);
	try {
		std::auto_ptr<sql::Statement> stmtAddRole(connectedToDB.con->createStatement());
		std::string str = "CALL update_role('" + appID + "','" + updatedRoleName + "','" + updatedStartDate + "','" + updatedEndDate
			+ "','" + updatedRiskLevel + "','" + updatedRoleType + "','" + updatedParentContainerId + "','" + updatedDescription + "')";
		stmtAddRole->execute(str);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}

	StringBuffer buffer;
	PrettyWriter<StringBuffer> writer(buffer);
	docJson.Accept(writer);

	return http::status::ok;
}


Roles::~Roles()
{
}

#pragma once
#include "CommonSources.hpp"
#include "HandleOperations.hpp"
#include "RestUtils.hpp"
using namespace rapidjson;

class Roles
{
public:
	Roles(const HandleOperations& con);
	//Roles(boost::beast::http::request<boost::beast::http::string_body> &req);
	~Roles();

	boost::beast::http::status getRole(Document &docJson, std::map<std::string, std::string> &queryParameters);
	/**
	* @brief: Delete the role by Id
	* @param docJson [out] : The ouput json file
	* @param roleId [in]: The role Id
	*/
	boost::beast::http::status deleteRole(Document &docJson, std::string &roleId);

	boost::beast::http::status addRole(const std::string &postBody, Document &docJson, std::string &locationField,
		const std::string &resource, std::string &errResponse);

	boost::beast::http::status updateRole(const std::string &postBody, Document &docJson, const std::string &appID);

	HandleOperations connectedToDB;
	RestUtils r;
};

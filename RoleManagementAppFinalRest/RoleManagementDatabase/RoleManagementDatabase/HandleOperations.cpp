#include "HandleOperations.hpp"

HandleOperations::HandleOperations()
{
	/* Create a connection */
	driver = get_driver_instance();
	con = driver->connect("tcp://127.0.0.1:3306", "root", "root");
	/* Connect to the MySQL database */
	con->setSchema("alexa");
}

void HandleOperations::getContainer() {
	pstmGetContainer = con->prepareStatement("SELECT id, description, name FROM parentcontainer");
	resGetContainer = pstmGetContainer->executeQuery();
	resGetContainer->afterLast();
	while (resGetContainer->previous())
	{
		std::cout << "\t Parent Container id: " << resGetContainer->getInt("id") << std::endl;
		std::cout << "\t Parent Container description: " << resGetContainer->getString("description") << std::endl;
		std::cout << "\t Parent Container name: " << resGetContainer->getString("name") << std::endl;
	}
}

void HandleOperations::addContainer() {
	try {
		pstmAddContainer = con->prepareStatement("INSERT INTO parentcontainer (description, name) VALUES ('some description', 'leading department')");
		resAddContainer = pstmAddContainer->executeQuery();
		while (resAddContainer->next()) {
			std::cout << "The following container has been added to your DB: ";
			std::cout << "\t Parent Container id: " << resAddContainer->getInt("id") << std::endl;
			std::cout << "\t Parent Container description: " << resAddContainer->getString("description") << std::endl;
			std::cout << "\t Parent Container name: " << resAddContainer->getString("name") << std::endl;
		}
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::addContainerStoredProcedure() {
	std::string description{};
	std::string containerName{};
	std::cout << "Enter the description for your new container: ";
	std::getline(std::cin, description);
	std::cout << "\nEnter the department name for your new container: ";
	std::getline(std::cin, containerName);
	try {
		std::auto_ptr<sql::Statement> stmtAddContainerStoredPr(con->createStatement());
		std::string str = "CALL add_parent_container('" + description + "', '" + containerName + "')";
		stmtAddContainerStoredPr->execute(str);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::updateParentContainer() {
	std::string updatedDescription{};
	std::string departmentName{};
	std::string id{};
	std::cout << "Enter the id of the container: ";
	std::getline(std::cin, id);
	std::cout << "Enter the updated description for your container: ";
	std::getline(std::cin, updatedDescription);
	std::cout << "\nEnter the updated department name for your the container: ";
	std::getline(std::cin, departmentName);
	try {
		std::auto_ptr<sql::Statement> stmtUpdatePC(con->createStatement());
		std::string str = "CALL update_parent_container(" + id + ",'" + updatedDescription + "', '" + departmentName + "')";
		stmtUpdatePC->execute(str);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::deleteContainerWithTransaction() {
	std::string id{};
	std::cout << "Enter the id of the container you want to delete: ";
	std::getline(std::cin, id);
	try {
		std::auto_ptr<sql::Statement> stmtDeletePC(con->createStatement());
		std::string str = "CALL delete_container_w_transactions(" + id + ")";
		stmtDeletePC->execute(str);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::getRole() {
	try {
		std::auto_ptr<sql::Statement> stmtGetRole(con->createStatement());
		stmtGetRole->execute("CALL get_role()");
		std::auto_ptr< sql::ResultSet > res;
		do {
			res.reset(stmtGetRole->getResultSet());
			while (res->next()) {
				std::cout << "Role Name: " << res->getString("role_name")
					<< " which belongs to the parent container: " << res->getString("parent_container_name")
					<< std::endl;
			}
		} while (stmtGetRole->getMoreResults());
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::addRole() {
	std::string roleName{}, startDate{}, endDate{}, riskLevel{}, roleType{}, parentContainerId{}, description{};
	std::cout << "Enter name of your new role: ";
	std::getline(std::cin, roleName);
	std::cout << "Enter the start date for the role: ";
	std::getline(std::cin, startDate);
	std::cout << "Enter the end date for the role: ";
	std::getline(std::cin, endDate);
	std::cout << "Enter the risk level for the role: ";
	std::getline(std::cin, riskLevel);
	std::cout << "Enter the role type for the role: ";
	std::getline(std::cin, roleType);
	std::cout << "Enter the parent container id for the role: ";
	std::getline(std::cin, parentContainerId);
	std::cout << "Enter the description for the role: ";
	std::getline(std::cin, description);
	try {
		std::auto_ptr<sql::Statement> stmtAddRole(con->createStatement());
		std::string str = "CALL add_role('" + roleName + "','" + startDate + "','" + endDate
			+ "','" + riskLevel + "','" + roleType + "','" + parentContainerId + "','" + description + "')";
		stmtAddRole->execute(str);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::updateRole() {
	std::string id{}, updatedRoleName{}, updatedStartDate{}, updatedEndDate{}, updatedRiskLevel{}, updatedRoleType{}, updatedParentContainerId{}, updatedDescription{};
	std::cout << "Enter the id of the role you want to update:";
	std::getline(std::cin, id);
	std::cout << "Enter the updated name for the role:";
	std::getline(std::cin, updatedRoleName);
	std::cout << "Enter the updated start date for the role:";
	std::getline(std::cin, updatedStartDate);
	std::cout << "Enter the updated end date for the role:";
	std::getline(std::cin, updatedEndDate);
	std::cout << "Enter the updated risk level for the role:";
	std::getline(std::cin, updatedRiskLevel);
	std::cout << "Enter the updated role type for the role:";
	std::getline(std::cin, updatedRoleType);
	std::cout << "Enter the updated parent container id for the role:";
	std::getline(std::cin, updatedParentContainerId);
	std::cout << "Enter the description for the role:";
	std::getline(std::cin, updatedDescription);
	try {
		std::auto_ptr<sql::Statement> stmtAddRole(con->createStatement());
		std::string str = "CALL update_role('" + id + "','" + updatedRoleName + "','" + updatedStartDate + "','" + updatedEndDate
			+ "','" + updatedRiskLevel + "','" + updatedRoleType + "','" + updatedParentContainerId + "','" + updatedDescription + "')";
		stmtAddRole->execute(str);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::deleteRole() {
	std::string id{};
	std::cout << "Enter the id of the role you want to delete:";
	std::getline(std::cin, id);
	try {
		std::auto_ptr<sql::Statement> stmtDeleteRole(con->createStatement());
		std::string str = "CALL delete_role(" + id + ")";
		stmtDeleteRole->execute(str);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::testTransaction()
{
	try {
		std::auto_ptr<sql::Statement> stmtTestTransaction(con->createStatement());
		stmtTestTransaction->execute("CALL test_transaction()");
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::getPermissionsForRole() {
	std::string id{};
	std::cout << "Enter the id of the role you want to get permission for:";
	std::getline(std::cin, id);
	try {
		std::auto_ptr<sql::Statement> stmtGetPermissionForRole(con->createStatement());
		std::string str = "CALL get_permissions_for_role(" + id + ")";
		stmtGetPermissionForRole->execute(str);
		std::auto_ptr< sql::ResultSet > res;
		do {
			res.reset(stmtGetPermissionForRole->getResultSet());
			while (res->next()) {
				std::cout << "For role with id: " << res->getInt("role_id") << " and with permission id: " << res->getInt("permission_id")
					<< " the following permissions are granted: " << res->getString("permission_name")
					<< std::endl;
			}
		} while (stmtGetPermissionForRole->getMoreResults());
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}


void HandleOperations::getRoleForPermission() {
	std::string id{};
	std::cout << "Enter the permission id to get the roles to which they are assigned:";
	std::getline(std::cin, id);
	try {
		std::auto_ptr<sql::Statement> stmtGetRoleForPermission(con->createStatement());
		std::string str = "CALL get_role_for_permission(" + id + ")";
		stmtGetRoleForPermission->execute(str);
		std::auto_ptr< sql::ResultSet > res;
		do {
			res.reset(stmtGetRoleForPermission->getResultSet());
			while (res->next()) {
				std::cout << "For permission with id: " << res->getInt("permission_id") << " and role id: " << res->getInt("role_id")
					<< " the following roles have this permission: " << res->getString("role_name")
					<< std::endl;
			}
		} while (stmtGetRoleForPermission->getMoreResults());
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::addPermissionAssignment() {
	std::string role_id{};
	std::cout << "Enter the id of the role to give permissions: ";
	std::getline(std::cin, role_id);
	std::string permission_id{};
	std::cout << "Enter the id of the permission assigned to the role: ";
	std::getline(std::cin, permission_id);
	try {
		std::auto_ptr<sql::Statement> stmtAddPermissionAssignment(con->createStatement());
		std::string str = "CALL add_permission_assignment(" + role_id + "," + permission_id + ")";
		stmtAddPermissionAssignment->execute(str);
		//"CALL add_permission_assignment(114, 200)");
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

void HandleOperations::deletePermissionAssignment() {
	std::string id{};
	std::cout << "Enter the id of the permission you want to be deleted: ";
	std::getline(std::cin, id);
	try {
		std::auto_ptr<sql::Statement> stmtDeletePermissionAssignment(con->createStatement());
		std::string str = "CALL delete_permission_assignment(" + id + ")";
		stmtDeletePermissionAssignment->execute(str);
	}
	catch (sql::SQLException &e) {
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
}

HandleOperations::~HandleOperations()
{
	delete resGetContainer, resAddContainer;
	delete stmt;
	delete con;
	delete pstmGetContainer, pstmAddContainer;
}

#pragma once
#include "CommonSources.hpp"
namespace http = boost::beast::http;
using namespace rapidjson;
class RestUtils
{
public:
	RestUtils() {}
	~RestUtils() {};
	static const std::string PAGE;
	static const std::string PAGE_SIZE;
	/**
	* @brief: Check if there is only numerical character
	* @param iParam [in]: The imput string to check
	* @return a map of query parmaters
	*/
	static std::map<std::string, std::string> parseQuery(const std::string& query);

	struct ParsedURI {
		std::string resource;
		std::map<std::string, std::string> query;   // everything after '?', possibly nothing
	};
	/**
	* @brief: Check if there is only numerical character
	* @param iParam [in]: The imput string to check
	* @return The parsed URI
	*/
	static ParsedURI parseURI(const std::string& url);

	bool isOnlyDigit(const std::string &iParam);

	void sendErrorMessage(Document &docJson, int errorCode, std::string errorMsg);

	bool isPageAndSizeNumber(std::map<std::string, std::string> &iQueryParameters, std::string &oPageSize, std::string &oPageNumber);

	std::vector<std::string> RestUtils::getPageSizeAndNumber(std::map<std::string, std::string> &iQueryParameters, std::string &oPageSize, std::string &oPageNumber);
};


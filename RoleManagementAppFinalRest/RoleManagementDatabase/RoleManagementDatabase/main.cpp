//#include <iostream>
//#include "HandleOperations.hpp"
//
//int main(void)
//{
//	int choice;
//	HandleOperations ho;
//	bool exit = false;
//	do {
//		std::cout << std::endl
//			<< " 1 - Get containers\n"
//			<< " 2 - Add container\n"
//			<< " 3 - Update container\n"
//			<< " 4 - Delete container\n"
//			<< " 5 - Get role\n"
//			<< " 6 - Add role\n"
//			<< " 7 - Update role\n"
//			<< " 8 - Delete role\n"
//			<< " 9 - Get permission for role\n"
//			<< " 10 - Get roles for permission\n"
//			<< " 11 - Add a role permission assignment\n"
//			<< " 12 - Delete role permission assignment\n"
//			<< " 13 - Exit\n"
//			<< " Enter your choice: ";
//		std::cin >> choice;
//		std::cin.get();
//		switch (choice) {
//		case 1:
//			ho.getContainer();
//			break;
//		case 2:
//			ho.addContainerStoredProcedure();
//			break;
//		case 3:
//			ho.updateParentContainer();
//			break;
//		case 4:
//			ho.deleteContainerWithTransaction();   
//			break;
//		case 5:
//			ho.getRole();
//			break;
//		case 6:
//			ho.addRole();
//			break;
//		case 7:
//			ho.updateRole();  //// verificaaa
//			break;
//		case 8:
//			ho.deleteRole();
//			break;
//		case 9:
//			ho.getPermissionsForRole();
//			break;
//		case 10:
//			ho.getRoleForPermission();
//			break;
//		case 11:
//			ho.addPermissionAssignment();
//			break;
//		case 12:
//			ho.deletePermissionAssignment();
//			break;
//		case 13:
//			exit = true;
//			break;
//		}
//
//	} while (exit != true);
//
//
//	return 0;
//}


#include "RestHttpResponse.hpp"

int main(int argc, char* argv[])
{
	try
	{
		// Check command line arguments.
		if (argc != 4)
		{
			std::cerr <<
				"Usage: http-server-sync <address> <port> <doc_root>\n" <<
				"Example:\n" <<
				"    http-server-sync 0.0.0.0 8080 .\n";
			return EXIT_FAILURE;
		}
		//---------------------PASS IP ADDRESS IN COMMAND LINE ---------------------------------
		auto const address = boost::asio::ip::make_address(argv[1]);
		auto const port = static_cast<unsigned short>(std::atoi(argv[2]));
		std::string const doc_root = argv[3];

		//---------------------SET IP ADDRESS FROM CODE INSTEAD---------------------------------

		//auto const address = boost::asio::ip::make_address("127.0.0.1");
		//auto const port = static_cast<unsigned short>(8080);
		//std::string const doc_root = ".";
		////auto const doc_root = std::make_shared<std::string>(".");

		// The io_context is required for all I/O
		boost::asio::io_context ioc{ 5 };

		std::cout << "Waiting for method request ...\n";
		// The acceptor receives incoming connections
		tcp::acceptor acceptor{ ioc,{ address, port } };
		for (;;)
		{
			// This will receive the new connection
			tcp::socket socket{ ioc };

			// Block until we get a connection
			acceptor.accept(socket);

			// Launch the session, transferring ownership of the socket
			std::thread{ std::bind(
				&do_session,
				std::move(socket),
				doc_root) }.detach();
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}
}



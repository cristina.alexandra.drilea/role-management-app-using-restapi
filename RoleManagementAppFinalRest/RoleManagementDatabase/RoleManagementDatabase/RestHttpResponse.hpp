#pragma once
#include "HandleOperations.hpp"
#include "RestUtils.hpp"
#include "Roles.hpp"
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
namespace http = boost::beast::http;    // from <boost/beast/http.hpp>
using namespace rapidjson;


// This function produces an HTTP response for the given
// request. The type of the response object depends on the
// contents of the request, so the interface requires the
// caller to pass a generic lambda for receiving the response.
template<class Body, class Allocator, class Send>
void handle_request(boost::beast::string_view doc_root, http::request<Body, http::basic_fields<Allocator>>&& req, Send&& send)
{
	// Returns a bad request response
	auto const bad_request = [&req](boost::beast::string_view why)
	{
		http::response<http::string_body> res{ http::status::bad_request, req.version() };
		res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
		res.set(http::field::content_type, "text/html");
		res.keep_alive(req.keep_alive());
		res.body() = why.to_string();
		res.prepare_payload();
		return res;
	};

	// Returns a not found response
	auto const not_found = [&req](boost::beast::string_view target)
	{
		http::response<http::string_body> res{ http::status::not_found, req.version() };
		res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
		res.set(http::field::content_type, "text/html");
		res.keep_alive(req.keep_alive());
		res.body() = "The resource '" + target.to_string() + "' was not found.";
		res.prepare_payload();
		return res;
	};

	// Returns a server error response
	auto const server_error = [&req](boost::beast::string_view what)
	{
		http::response<http::string_body> res{ http::status::internal_server_error, req.version() };
		res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
		res.set(http::field::content_type, "text/html");
		res.keep_alive(req.keep_alive());
		res.body() = "An error occurred: '" + what.to_string() + "'";
		res.prepare_payload();
		return res;
	};

	// HTTP reponse
	http::response<http::string_body> res{ http::status::ok, req.version() };

	// Prepare the HTTP reponse
	auto const prepare_response = [&req, &res](Document &docJson)
	{
		StringBuffer bufferJson;
		Writer<StringBuffer> writer(bufferJson);
		docJson.Accept(writer);
		std::string jsonResponse(bufferJson.GetString(), bufferJson.GetSize());
		res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
		res.set(http::field::content_type, "application/json");
		res.set(http::field::access_control_allow_origin, "*");
		res.body() = jsonResponse;
		res.content_length(jsonResponse.size());
		res.keep_alive(req.keep_alive());
		return res;
	};

	// Make sure we can handle the method
	if (req.method() != http::verb::get &&
		req.method() != http::verb::post &&
		req.method() != http::verb::patch &&
		req.method() != http::verb::delete_)
		return send(bad_request("Unknown HTTP-method"));

	RestUtils::ParsedURI parsedURI = RestUtils::parseURI(req.target().to_string());
	std::vector<std::string> resources;
	std::istringstream f(parsedURI.resource.c_str());
	std::cout << "your URI: " << req.target().to_string() << '\n';
	std::string s;
	while (std::getline(f, s, '/')) {
		//std::cout << s << std::endl;
		resources.push_back(s);
	}

	HandleOperations ct;

	// Check ressources
	if (resources.size() <= 2 || resources.at(0) != "" || resources.at(2) != "v1")
		return send(not_found("Unknown resource"));

	// Respond to GET request
	if (req.method() == http::verb::get)
	{
		Document docJson;
		if (resources.at(3) == "roles") {
			Roles roles(ct);
			if (resources.size() == 4) {
				// get roles
				res.result(roles.getRole(docJson, parsedURI.query));
			}
		}
		else
			return send(not_found(parsedURI.resource.c_str()));
		//prepare response
		return send(std::move(prepare_response(docJson)));
	}

	// Respond to Delete request
	if (req.method() == http::verb::delete_)
	{
		Document docJson;
		if (resources.at(3) == "roles")
		{
			// delete role by id
			Roles roles(ct);
			if (resources.size() == 5)
				res.result(roles.deleteRole(docJson, resources.at(4)));
		}
		else
			return send(not_found(parsedURI.resource.c_str()));
		//prepare response
		return send(std::move(prepare_response(docJson)));
	}


	// Respond to POST request
	if (req.method() == http::verb::post)
	{
		Document docJson;
		std::string locationField("");
		std::string errResponse("");
		if (resources.at(3) == "roles") {
			Roles roles(ct);
			if (resources.size() == 4) {
				auto postBody = req.body();
				res.result(roles.addRole(postBody, docJson, locationField, parsedURI.resource, errResponse));
			}
			else {
				return send(not_found(parsedURI.resource.c_str()));
			}
			//prepare response
			if (!locationField.empty())
				res.set(http::field::location, locationField);
			//prepare response
			return send(std::move(prepare_response(docJson)));
		}
	}

	// Respond to PATCH request
	if (req.method() == http::verb::patch)
	{
		Document docJson;
		std::string errResponse("");
		if (resources.at(3) == "roles") {
			Roles roles(ct);
			if (resources.size() == 5) {
				auto postBody = req.body();
				res.result(roles.updateRole(postBody, docJson, resources.at(4)));
			}
			else {
				return send(not_found(parsedURI.resource.c_str()));
			}
			//prepare response
			return send(std::move(prepare_response(docJson)));
		}
	}





	//// Request path must be absolute and not contain "..".
	//if (req.target().empty() ||
	//	req.target()[0] != '/' ||
	//	req.target().find("..") != boost::beast::string_view::npos)
	//	return send(bad_request("Illegal request-target"));

	//// Build the path to the requested file
	//std::string path = path_cat(doc_root, req.target());
	//if (req.target().back() == '/')
	//	path.append("index.html");

	//// Attempt to open the file
	boost::beast::error_code ec;
	http::file_body::value_type body;
	//	body.open(path.c_str(), boost::beast::file_mode::scan, ec);

	// Handle the case where the file doesn't exist
	if (ec == boost::system::errc::no_such_file_or_directory)
		return send(not_found(req.target()));

	// Handle an unknown error
	if (ec)
		return send(server_error(ec.message()));

	// Cache the size since we need it after the move
	auto const size = body.size();


}

//------------------------------------------------------------------------------

// Report a failure
void fail(boost::system::error_code ec, char const* what)
{
	std::cerr << what << ": " << ec.message() << "\n";
}

// This is the C++11 equivalent of a generic lambda.
// The function object is used to send an HTTP message.
template<class Stream>
struct send_lambda
{
	Stream& stream_;
	bool& close_;
	boost::system::error_code& ec_;

	explicit
		send_lambda(
			Stream& stream,
			bool& close,
			boost::system::error_code& ec)
		: stream_(stream)
		, close_(close)
		, ec_(ec)
	{
	}

	template<bool isRequest, class Body, class Fields>
	void
		operator()(http::message<isRequest, Body, Fields>&& msg) const
	{
		// Determine if we should close the connection after
		close_ = msg.need_eof();

		// We need the serializer here because the serializer requires
		// a non-const file_body, and the message oriented version of
		// http::write only works with const messages.
		http::serializer<isRequest, Body, Fields> sr{ msg };
		http::write(stream_, sr, ec_);
	}
};

// Handles an HTTP server connection
void do_session(tcp::socket& socket, std::string const& doc_root)
{
	bool close = false;
	boost::system::error_code ec;

	// This buffer is required to persist across reads
	boost::beast::flat_buffer buffer;

	// This lambda is used to send messages
	send_lambda<tcp::socket> lambda{ socket, close, ec };

	for (;;)
	{
		// Read a request
		http::request<http::string_body> req;
		http::read(socket, buffer, req, ec);
		if (ec == http::error::end_of_stream)
			break;
		if (ec)
			return fail(ec, "read");

		// Send the response
		handle_request(doc_root, std::move(req), lambda);
		if (ec)
			return fail(ec, "write");
		if (close)
		{
			// This means we should close the connection, usually because
			// the response indicated the "Connection: close" semantic.
			break;
		}
	}

	// Send a TCP shutdown
	socket.shutdown(tcp::socket::shutdown_send, ec);

	// At this point the connection is closed gracefully
}

#include "RestUtils.hpp"

const std::string RestUtils::PAGE = "page";
const std::string RestUtils::PAGE_SIZE = "pageSize";

std::map<std::string, std::string> RestUtils::parseQuery(const std::string& query)
{
	std::map<std::string, std::string> data;
	std::regex pattern("([\\w+%]+)=([^&]*)");
	auto words_begin = std::sregex_iterator(query.begin(), query.end(), pattern);
	auto words_end = std::sregex_iterator();

	for (std::sregex_iterator i = words_begin; i != words_end; i++)
	{
		std::string key = (*i)[1].str();
		std::string value = (*i)[2].str();
		data[key] = value;
	}

	return data;
}

RestUtils::ParsedURI RestUtils::parseURI(const std::string& url) {
	ParsedURI result;
	auto value_or = [](const std::string& value, std::string&& deflt) -> std::string {
		return (value.empty() ? deflt : value);
	};
	// Note: only "http", "https", "ws", and "wss" protocols are supported
	static const std::regex PARSE_URL{ R"(^(([^:\/?#]+):)?(//([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?)",
		std::regex_constants::ECMAScript | std::regex_constants::icase };
	std::smatch match;
	if (std::regex_match(url, match, PARSE_URL)) {
		result.resource = value_or(match[5], "/");
		result.query = parseQuery(match[7]);
	}
	return result;
}

bool RestUtils::isOnlyDigit(const std::string &iParam)
{
	return iParam.find_first_not_of("0123456789") == std::string::npos;
}

void RestUtils::sendErrorMessage(Document &docJson, int errorCode, std::string errorMsg) {
	docJson.SetObject();
	Document::AllocatorType& allocator = docJson.GetAllocator();
	Value jsonString(kStringType);
	std::string errCode(std::to_string(errorCode));
	jsonString.SetString(errCode.c_str(), allocator);
	docJson.AddMember("Status Code", jsonString, allocator);
	jsonString.SetString(errorMsg.c_str(), allocator);
	docJson.AddMember("Message", jsonString, allocator);
}


bool RestUtils::isPageAndSizeNumber(std::map<std::string, std::string> &iQueryParameters, std::string &oPageSize, std::string &oPageNumber) {
	std::map<std::string, std::string>::iterator itSize = iQueryParameters.find(PAGE_SIZE);
	oPageSize = (itSize != iQueryParameters.end() ? itSize->second : "");
	std::map<std::string, std::string>::iterator itPage = iQueryParameters.find(PAGE);
	oPageNumber = (itPage != iQueryParameters.end() ? itPage->second : "");
	if (!isOnlyDigit(oPageSize) || !isOnlyDigit(oPageNumber))
	{
		return false;
	}
	return true;
}

std::vector<std::string> RestUtils::getPageSizeAndNumber(std::map<std::string, std::string> &iQueryParameters, std::string &oPageSize, std::string &oPageNumber) {
	std::vector<std::string> resultVec;
	std::map<std::string, std::string>::iterator itSize = iQueryParameters.find(PAGE_SIZE);
	oPageSize = (itSize != iQueryParameters.end() ? itSize->second : "");
	std::map<std::string, std::string>::iterator itPage = iQueryParameters.find(PAGE);
	oPageNumber = (itPage != iQueryParameters.end() ? itPage->second : "");
	resultVec.push_back(oPageNumber);
	resultVec.push_back(oPageSize);

	return resultVec;
}
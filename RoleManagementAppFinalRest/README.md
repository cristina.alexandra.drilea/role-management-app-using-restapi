This application is written in C++. It handles the roles that exist in an organization.
The roles,all the assigned permissions, business codes and parent containers are kept inside different tables created using MySQL Workbench. The entire schema and the stored procedures can be found inside the repository in alexaDatabase.mysql.
Basically the app connects to the database on port 3306 and calls the stored procedure for each type of operation. Some of the stored procedures are made using transactions in case one of the operation fails (let's say if a delete request fails) then a rollback will be done. Also many foreign keys were created between the tables with cascade option on delete restricted.
In order to retrieve data from the alexaDatabase API calls are made from Postman. All operations are possible: retrieve list of roles (GET), update role (PATCH), delete role (DELETE), add role (POST). 
USAGE:
URI for GET: http://localhost:8080/api/v1/roles/  -> will retrieve all the roles from the database

			 http://localhost:8080/api/v1/roles/roleId  -> will retrieve all the details for a certain role; replace roleId with the id of the role ex. 111
			 
	for DELETE: http://localhost:8080/api/v1/roles/roleId -> will delete the role with id roleId from the db; set the method to be on DELETE
	
	for PATCH: http://localhost:8080/api/v1/roles/roleId -> will update the details for the role provided by it's id (roleId) and the updated values must be sent
															from body inside Postman (same as for POST)
															if the updated values should be sent from keyboard uncomment the lines from 281 to 314 in Roles.cpp
															
	for POST: http://localhost:8080/api/v1/roles -> !! The details must be sent from body inside Postman 
													go to Body -> select raw and add the details for the role ex:
															{
																"name":" application engineer",
																"start_date": "2020-04-04",
																"end_date":  "2021-08-08",
																"risk_level": "low",
																"role_type": "tehnical",
																"parent_container_id":  "112",
																"description": "engineer"
															}
													in the response in Postman the status code 201 Created will be returned and the role recently added:
															{
																"role": [
																	{
																		"roleName": " engineer 2",
																		"startDate": "2020-04-04",
																		"endDate": "2019-08-08",
																		"riskLevel": "low",
																		"roleType": "tehnical",
																		"parentContainerId": "112",
																		"description": "engineer"
																	}
																]
															}
													If you want to introduce the attributes and values from keyboard please uncomment the lines from 151 to 182 in Roles.cpp